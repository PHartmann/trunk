# -*- coding: utf-8 -*-

"""
Tension test of a bonded particle packing using peridynamics 

Author: Philipp Hartmann
"""
from yade import plot,export,ymport, pack
import numpy as np

#### create output directories
try:
	os.mkdir('gnuplot')
except:
	pass
try:
	os.mkdir('paraview')
except:
	pass

#### PARAMETERS

## PD material parameters
youngPD = 6e6
nuPD = 0.4
densityPD = 8100
gcPD = 5e0

#btype = "bending"
#btype = "tension"
btype = "2particles"

#### GEOMETRY
sphereRadius = 2.0 #(=0.5*particle spacing)
## Peridynamic quantaties
#interaction radius for creating the initial bonds
m=1
horizonPD = m*(sphereRadius*2) # Real peridynamic horizon 
#intRadius=horizon*1.01
intRadius =(horizonPD/4)*1.01 # divided by 4 since horizon is sphere radius times 2 and for PD the distance of centres of of interest and not contact of spheres 

### MATERIAL
# ADD ELASTIC PD MATERIAL 
O.materials.append(
  PdMat(
  young = youngPD,
  poisson = nuPD,
  density = densityPD,
  horizon = horizonPD,
  gc = gcPD,
  label='MatPD'))

#sp = pack.regularOrtho(pack.inAlignedBox((0,0,0),(100,50,20)),radius=sphereRadius,gap=0,color=(0,0,1), material = 'MatPD')
#sp = pack.regularOrtho(pack.inAlignedBox((0,0,0),(25,25,5)),radius=sphereRadius,gap=0,color=(0,0,1), material = 'MatSpheres')
#O.bodies.append(sp)
r = 1
O.bodies.append(sphere([0.,0.,sphereRadius],r,color=[1,0,0],wire=True,material='MatPD')) 
O.bodies.append(sphere([sphereRadius,0.,sphereRadius],r,color=[0,1,0],wire=True,material='MatPD')) 


#### Boundaries (directions related to regularOrtho (x,y,z)) get all particle of a regular geometry  
# x-axis min, max particles 
bb=uniaxialTestFeatures(axis=0)
minX,maxX = bb['negIds'],bb['posIds']
# y-axis min, max particles 
bb=uniaxialTestFeatures(axis=1)
minY,maxY  = bb['negIds'],bb['posIds']
# z-axis min, max particles 
bb=uniaxialTestFeatures(axis=2)
minZ, maxZ  = bb['negIds'],bb['posIds']

### Extend boundary layers () for PD computations 
# minX 
for i in range (len(minX)*(m-1)):
    minX.append(minX[-1]+1)
# maxX 
maxX.reverse()
for i in range (len(maxX)*(m-1)):
    maxX.append(maxX[-1]-1)    
    

#### UNIAXIAL TEST FEATURE
bb=uniaxialTestFeatures(axis=0)
negIds,posIds,axis,crossSectionArea=bb['negIds'],bb['posIds'],bb['axis'],bb['area']

## change colour of moving particles negIds and posIds
for i in minX:
	O.bodies[i].shape.color=[1,0,0]
for i in maxX:
	O.bodies[i].shape.color=[1,0,0]

# Function replacing the sort collinder since bonds have to be defined only once        
def initializeBonds(intRadius):
    #print "initializeBonds"
    O.step() 
    # adjust volumes #FIXME
    ## check number of interactions
    num = 0
    for i in O.interactions:
        if i.isReal :
            num = num+1
            #if i.id1==20 or i.id2==20:
               #print i.id1, " ", i.id2
               
    print ("Number of interactions:", num)
    print ("Number of possible interactions:", len(O.interactions))
    print ("Number of particles:", len(O.bodies))
        

def getBoundaries(btype):
    if(btype=="bending"):
        print("Computation of bending problem")
        # Loop over all boundary particles in x-direction
        for i in minX:
            O.bodies[i].state.blockedDOFs = 'xyz'
        for i in maxX:
            O.bodies[i].state.blockedDOFs = 'xyz'
    elif(btype=="punch"):
        print("Computation of punch problem")
    elif(btype=="tension"):
        print("Computation of tension problem")   
        # Loop over all boundary particles in x-direction
        for i in minX:
            O.bodies[i].state.blockedDOFs = 'xyz'
        for i in maxX:
            O.bodies[i].state.blockedDOFs = 'xyz'
    elif(btype=="2particles"):
        print("Computation of simple problem using 2 particles")  
        # Loop over all boundary particles in x-direction
        for i in minX:
            O.bodies[i].state.blockedDOFs = 'xyz'
        for i in maxX:
            O.bodies[i].state.blockedDOFs = 'xyz'        
    else:
        print("Boundaries not defined")
    # not implemented yet     


### iteration number for boundary 
iteration = 0
stopBound = 0;

def applyBoundaries(btype):
    if(btype=="bending"):
        vel = 50
        for i in maxX:
            O.bodies[i].state.vel[1] =  -vel
            
    elif(btype=="punch"):
        a=2
    elif(btype=="tension"):
        # Define velocity 
        #disp = strainRate*abs(O.bodies[maxX[0]].state.refPos[0]-O.bodies[minX[0]].state.refPos[0])
        vel = 50
        for i in minX:
            #O.bodies[i].state.pos[0] = O.bodies[i].state.pos[0] - disp
            O.bodies[i].state.vel[0] = -vel
        for i in maxX:
            #O.bodies[i].state.pos[0] = O.bodies[i].state.pos[0] + disp
            O.bodies[i].state.vel[0] =  vel
    elif(btype=="2particles"):
        # Define velocity 
        vel = 1e-3
        
        global iteration
        global stopBound
        
        iteration = iteration + 1
        
        if stopBound == 1: 
            O.bodies[0].dynamic = True
            O.bodies[1].dynamic = True
            return
        
        if iteration == 300: # and iteration < 20: 
            vel = -vel
            stopBound = 1
            
        for i in minX:
            #O.bodies[i].state.pos[0] = O.bodies[i].state.pos[0] - disp
            O.bodies[i].state.vel[0] = -vel
        for i in maxX:
            #O.bodies[i].state.pos[0] = O.bodies[i].state.pos[0] + disp
            O.bodies[i].state.vel[0] =  vel
    else:
        print("btype not found")
        a=2 

#### ENGINES 
O.engines=[
	ForceResetter(),
	InsertionSortCollider([Bo1_Sphere_Aabb(aabbEnlargeFactor=intRadius,label='is2aabb')]),
	InteractionLoop(
		[Ig2_Sphere_Sphere_ScGeom(interactionDetectionFactor=intRadius,label='ss2sc')],
		[Ip2_PdMat_PdMat_PdPhys()],
		[Law2_ScGeom_PdPhys_PdDummy(fracture=True,friction=True)]),
    PDEngine(appliedPDModel=1),
    #Apply boundary conditions manually 
    PyRunner(iterPeriod=1000,initRun=False,command='applyBoundaries(btype)',label='boundayApplication'),
    # Solve dynamics
    NewtonIntegrator(label='newton'), 
]
O.dt=1e-5
       

       
       
##Calculate number of interactions initially
#O.step()
#totBond = init()
##Peridynamic initialisation
initializeBonds(intRadius)
#strainer.dead=False
is2aabb.aabbEnlargeFactor=0
ss2sc.interactionDetectionFactor=0

#Define boundary particles globally  
getBoundaries(btype)

yade.qt.Controller()
yade.qt.View()
renderer=yade.qt.Renderer()
#renderer.dispScale=(10,1,0)  # scale displacement in GUI
renderer.bgColor=(1,1,1)


#plot.plots={'eps':('sigma')}
#plot.plot(noShow = False, subPlots=False)

O.saveTmp()
