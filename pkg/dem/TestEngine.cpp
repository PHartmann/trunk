//#ifdef YADE_PD
#include<pkg/dem/TestEngine.hpp>
#include<core/Scene.hpp>

namespace yade { // Cannot have #include directive inside.
    YADE_PLUGIN((TestEngine));


void TestEngine::updateForces()
{
  const int nB = (int)scene->bodies->size();
  forces = MatrixXr::Zero(nB,3);
  std::cout << "Update forces test class" << '\n';
}

// void TestEngine::action()
// {
//   std::cout << "TestEngine action()" << '\n';
// }

} // namespace yade

//#endif
