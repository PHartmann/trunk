//#ifdef YADE_PD
#include<core/Scene.hpp>
#include<core/State.hpp>
#include<pkg/dem/NumericalMethodEngine.hpp>

namespace yade { // Cannot have #include directive inside.
YADE_PLUGIN((NumericalMethodEngine));

void NumericalMethodEngine::action()
{
  //std::cout << "Numerical Method Action called" << '\n';
  // check if coupled within CouplingEngine, if not perform normal action as defined in NumericalMethodEngine
  if(!initialisedInCouplingEngine)
  {
    normalAction();
  }
  else
  {
    // do nothing, everything is handled within CoupledEngine
  }

}

void NumericalMethodEngine::updateForces()
{
  //FIXME should not be called
  std::cout<<"\033[1;31mupdateForces from NumericalMethodEngine is called. Please check if method is implemented in applied derived class : Exit code \033[0m\n"<<std::endl;
  exit(0);
}

std::vector<int> NumericalMethodEngine::getHybridParticles()
{
  //FIXME should not be called
  std::cout<<"\033[1;31getHybridParticles from NumericalMethodEngine is called. Please check if method is implemented in applied derived class : Exit code \033[0m\n"<<std::endl;
  exit(0);
  std::vector<int> result;
  return result;
}

void NumericalMethodEngine::initialiseFixedConnections()
{
  //FIXME should not be called
  std::cout<<"\033[1;31muinitialiseFixedConnections from NumericalMethodEngine is called. Please check if method is implemented in applied derived class : Exit code \033[0m\n"<<std::endl;
  exit(0);
}

void NumericalMethodEngine::normalAction()
{
    updateForces();
    //std::cout << "normal Action forces updated " << '\n';
    addForcesToScene();
    //std::cout << "normal Action forces added " << '\n';
}

// void NumericalMethodEngine::methodAction()
// {
//   std::cout << "Testsss NumericalMethodEngine" << '\n';
//   std::cout << "Time step size: " << timeStepSize << '\n';
//   std::cout << "Time step factor higher: " << timeStepFactorHigher << '\n';
//   std::cout << "Time step factor smaller: " << timeStepFactorSmaller << '\n';
//   std::cout << " " << '\n';
// }

void NumericalMethodEngine::setTimeStepInformation()
{
  // no input given, i.e. time step size is equivalent to DEM time step
  if(timeStepFactorHigher== 1 && timeStepFactorSmaller == 1 && timeStepSize == 0)
  {
    return;
  }

  // Catch errors from user input
  if(timeStepFactorHigher!= 1 && timeStepFactorSmaller != 1)
  {
    std::cout << timeStepFactorHigher << "left higher, right higher" << timeStepFactorSmaller<<'\n';
    std::cout<<"\033[1;31mTime step for numerical method can't be smaller and bigger than the DEM time step at the same time : Exit code \033[0m\n"<<std::endl;
    exit(0);
  }
  if((timeStepFactorHigher!= 1 || timeStepFactorSmaller != 1) && timeStepSize != 0)
  {
    std::cout<<"\033[1;31mBoth, time step size and a time step factor are used in the argument for the numerical method. Choose only one : Exit code \033[0m\n"<<std::endl;
    exit(0);
  }

  if(timeStepFactorHigher!= 1)
  {
    timeStepSize = scene->dt*timeStepFactorHigher;
    return;
  }

  if(timeStepFactorSmaller!= 1)
  {
    timeStepSize = scene->dt*(1.0/timeStepFactorHigher);
    return;
  }
  // last possibility: time step size for numerical method is given
  Real const fac = scene->dt/timeStepSize;
  if(fac<=1)
  {
    timeStepFactorHigher = (int) (1.0/fac);
  }
  else
  {
    timeStepFactorSmaller = (int) fac;
  }

}

void NumericalMethodEngine::addForcesToScene()
{
  for(int i=0;i<forces.rows();i++)
    scene->forces.addForce(i,forces.row(i));
}

} // namespace yade

//#endif
