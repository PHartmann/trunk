//#ifdef YADE_PD
#pragma once

#include<core/PartialEngine.hpp>
#include<pkg/dem/NumericalMethodEngine.hpp>
#include<pkg/dem/PeriMat.hpp>
#include<pkg/dem/ScGeom.hpp>

namespace yade { // Cannot have #include directive inside.


enum MaterialModels {BondBased=1,StateBased=2,StateBasedNL=3};
//"Type of material model undefined! The following models are available: BondBased=1 , StateBased=2 , StateBasedNL=3 .")
#define MODELFUNCDESCR throw runtime_error("Type of Model undefined! The following kernel functions are available: BondBased=1 , StateBased=2 , StateBasedNL=3.");

class PDEngine: public NumericalMethodEngine{
  public:
    //virtual void action();
    void updateForces() override final;
    std::vector<int> getHybridParticles()  override final;
    void initialiseFixedConnections()  override final;
    
    void computeBondBasedForces(const shared_ptr<PdMat>& mat);
    void computeStateBasedForces(const shared_ptr<PdMat>& mat);
    void computeStateBasedForcesNL(const shared_ptr<PdMat>& mat);
    void computeStresses(const shared_ptr<PdMat>& mat);
    void assembleForces(std::vector<MatrixXr> &Tij_all, const shared_ptr<PdMat>& mat);
//    bool bondBroken(const shared_ptr<PdPhys>& physics);
    void applyForceBoundriesAsDensities(std::vector<MatrixXr> &Tij_all);
    void applyForceBoundriesAsForces(const Real density);


	// clang-format off
  YADE_CLASS_BASE_DOC_ATTRS(PDEngine,NumericalMethodEngine,"Apply given torque (momentum) value at every subscribed particle, at every step. ",
    ((int,appliedPDModel, BondBased,, "Material models for force computation (by default - Bond based). The following models are available: BondBased=1 , StateBased=2 , StateBasedNL=3 ."))
    ((bool,fracture, false,, "If true bond breakage is considered"))
    ((std::vector<int>,forceDensityNodes,,, "Vector of body integers subjected to a fixed force density"))
    ((Vector3r,forceDensityApplied,Vector3r::Zero(),, "Vector of body integers subjected to a fixed force density"))
  );
	// clang-format on
};
REGISTER_SERIALIZABLE(PDEngine);


} // namespace yade

//#endif
