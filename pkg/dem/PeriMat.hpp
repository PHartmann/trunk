
/*************************************************************************
*  Copyright (C) 2020 by Philipp Hartmann                                *
*  philipp.hartmannn@newcastle.edu.au                                     *
*                                                                        *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/

/**
=== OVERVIEW OF PeriMat ===

A peridynamic material model for friction and viscous damping in normal direction. The damping coefficient
has to be defined with the material whereas the contact stiffness kn and ks/kn can be defined with the Ip2 functor.
*/

#pragma once

#include<pkg/common/ElastMat.hpp>
#include<pkg/common/Dispatching.hpp>
#include<pkg/dem/ScGeom.hpp>
#include<core/IPhys.hpp>
#include<lib/base/openmp-accu.hpp>
//#include<pkg/common/NormShearPhys.hpp>
#include <pkg/dem/FrictPhys.hpp>
#include<core/Shape.hpp>

#include <pkg/common/Aabb.hpp>


#include<core/Scene.hpp>
#include<core/Omega.hpp>
#include<pkg/common/InteractionLoop.hpp>


namespace yade { // Cannot have #include directive inside.


/** This class holds information associated with each body state*/
class PDState: public State {
	// clang-format off
	YADE_CLASS_BASE_DOC_ATTRS_CTOR(PDState,State,"PD state information about each body.",
        ((int,m,3,,"Horizon/dx. [-]")) //FIXME might be deleted later on
			((Real,volPD,0.,,"Peridynamic volume"))	
		((int,nbInitBonds,0,,"Number of initial bonds. [-]"))
		((int,nbBrokenBonds,0,,"Number of broken bonds. [-]"))
    ((std::vector<int>,neighbours,,,"Vector of neighbouring particles"))
        //((vector<int>,neighbours,,,"Vector storing all cohesive neighbour ids. When bond breaks, the neighbour id is deleted from list"))
		,
		createIndex();
	);
	// clang-format on
	REGISTER_CLASS_INDEX(PDState,State);
};
REGISTER_SERIALIZABLE(PDState);


/** This class holds information associated with each body */
class PdMat: public  FrictMat{
	public:
		virtual shared_ptr<State> newAssocState() const { return shared_ptr<State>(new PDState); }
		virtual bool stateTypeOk(State* s) const { return (bool)dynamic_cast<PDState*>(s); }
	// clang-format off
		YADE_CLASS_BASE_DOC_ATTRS_CTOR(PdMat,FrictMat,"Material for use with the peridynamic classes",
			((Real,horizon,0.,,"Horizon of family"))
            ((Real,gc,0.0,,"GC-value for the computation of critical bond stretch"))
            ,
			createIndex();
		);
	// clang-format on
		DECLARE_LOGGER;
		REGISTER_CLASS_INDEX(PdMat,FrictMat);
};
REGISTER_SERIALIZABLE(PdMat);

/** This class defines a PD physics used for PD computations */
class PdPhys:public FrictPhys{
	public:
		virtual ~PdPhys() {};
	// clang-format off
	YADE_CLASS_BASE_DOC_ATTRS_CTOR(PdPhys,FrictPhys,"Abstract class for interactions whereby all interactions within the horizon are real.",
        //((bool,isBroken,false,,"True when bond is broken"))
        //((Real,tangensOfFrictionAngle,NaN,,"tan of angle of friction"))
        //((Real,FsMax,0.,,"Maximal shear force"))
        ,
		createIndex();
	);
	// clang-format on
    DECLARE_LOGGER;
	REGISTER_CLASS_INDEX(PdPhys,FrictPhys);
};
REGISTER_SERIALIZABLE(PdPhys);

enum FrictionPhysics {simpleFriction=1}; // Extendable if necessary
/** 2d functor creating IPhys (Ip2) taking PdMat and PdMat of 2 bodies, returning type PdPhys */
class Ip2_PdMat_PdMat_PdPhys: public IPhysFunctor{
	public:
		virtual void go(const shared_ptr<Material>& pp1, const shared_ptr<Material>& pp2, const shared_ptr<Interaction>& interaction);
		FUNCTOR2D(PdMat,PdMat);
	// clang-format off
		YADE_CLASS_BASE_DOC_ATTRS(Ip2_PdMat_PdMat_PdPhys,IPhysFunctor,"Converts 2 :yref:`PdMat` instances to :yref:`PdPhys`",
        //((int,iniSteps,10,,"Define the number of initialization steps in which the material physics are exclusively defined as PdPhys. Afterwards an Ip2 functor related to DEM frictional contact is used"))
        //((int,frictionalPhysics,simpleFriction,,"Defines DEM frictional contact physics, respectively which IP2 functor is additionally called"))
        );
	// clang-format on
		DECLARE_LOGGER;
};
REGISTER_SERIALIZABLE(Ip2_PdMat_PdMat_PdPhys);


/** 2d functor creating the interaction law (Law2) based on SphereContactGeometry (ScGeom) and PdPhys of 2 bodies, returning type dummy */
class Law2_ScGeom_PdPhys_PdDummy: public LawFunctor{
	public:
		virtual bool go(shared_ptr<IGeom>& _geom, shared_ptr<IPhys>& _phys, Interaction* I);
        Real getCriticalStretch(const shared_ptr<PdMat>& mat);
	// clang-format off
		YADE_CLASS_BASE_DOC_ATTRS_CTOR_PY(Law2_ScGeom_PdPhys_PdDummy,LawFunctor,"Dummy constitutive law for peridynamic simulations. No forces are added.",
        ((bool,fracture,false,,"True if fracture is considered via critical stretch and associated bond breakage"))
        //((bool,friction,false,,"True if frictional contact between broken bonds is considered"))
        ,,)
        ;
	// clang-format on
		FUNCTOR2D(ScGeom,PdPhys);
		DECLARE_LOGGER;
};
REGISTER_SERIALIZABLE(Law2_ScGeom_PdPhys_PdDummy);

} // namespace yade
