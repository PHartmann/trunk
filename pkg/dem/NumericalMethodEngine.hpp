//#ifdef YADE_PD
#pragma once
#include<core/PartialEngine.hpp>

namespace yade { // Cannot have #include directive inside.

enum CouplingModels {conformingStaggered=1,nonConformingStaggered=2};
#define COUPLEMODELFUNCDESCR throw runtime_error("Type of Coupling undefined! The following kernel functions are available: conformingStaggered=1,nonConformingStaggered=2.");

class NumericalMethodEngine: public PartialEngine{
  public:
    virtual void action();
    // virtual void updateForces()=0;
    // virtual void normalAction()=0; // redefines the classical action() function performing the required steps
    virtual void updateForces();
    virtual void normalAction(); // could be overwritten if necessary
    void addForcesToScene();
    void setTimeStepInformation();
    MatrixXr forces;

    virtual std::vector<int> getHybridParticles(); // return list of all hybrid particles when CouplingEngine used
    virtual void initialiseFixedConnections();

    std::map<int,int> numFixedConnections; // BodyId, number of fixed connections (family in PD)
    std::map<int,Vector3r> storedVel; // BodyId, associated velocity
    // bool initialisedInCouplingEngine = false;
    // Real timeStepSize = 0.0;
    //
    // int stepsUntilForceUpdate = 1; // 1 means it will updated. Example when 10, it won't be updated 9 times (decreased in action())
    // int timeStepFactorHigher = 1;
    // int timeStepFactorSmaller = 1;
	// // clang-format off
  YADE_CLASS_BASE_DOC_ATTRS(NumericalMethodEngine,PartialEngine,"Abstract class to use for the implementation of new numerical methods. Gives the possibility to use CoupledEngine, i.e. the usage of different time step sizes within the coupling of varies methods",
  ((bool,initialisedInCouplingEngine,  false,, "Bool defining if pointer of engine is used in CouplingEngine. Necessary to check for initialisation"))
  ((int,timeStepFactorHigher,  1,, "In comparison to DEM the time step of the numerical method is ... times higher."))
  ((int,timeStepFactorSmaller, 1,, "In comparison to DEM the time step of the numerical method is ... times smaller."))
  ((int,stepsUntilForceUpdate, 0,, "1 means it will updated. Example when 10, it won't be updated 9 times (decreased in action())."))
  ((Real,timeStepSize, 0.0,, "Time step size of numerical method."))
  ((int,coupling, conformingStaggered,, "Coupling possibilities (by default - conformingStaggered). The following couplings are available: conformingStaggered=1 , nonConformingStaggered=2."))
  //((MatrixXr,forces,,,"Force container in which the resulting forces from the numerical method are stored."))
   )
	// clang-format on
};
REGISTER_SERIALIZABLE(NumericalMethodEngine);


} // namespace yade

//#endif
