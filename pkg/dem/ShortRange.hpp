#include<core/Material.hpp>
#include<core/IPhys.hpp>
#include<pkg/dem/ScGeom.hpp>
#include<pkg/common/Dispatching.hpp>
#include<pkg/dem/PeriMat.hpp>

namespace yade { // Cannot have #include directive inside.

/********************** Short Range  ****************************/
class ShortRangePhys : public IPhys {
    public:
	virtual ~ShortRangePhys(){};
	// clang-format off
    YADE_CLASS_BASE_DOC_ATTRS_CTOR(ShortRangePhys,IPhys,"Physics of short range interactions, for use with PeriMat",,
	createIndex();
	);
	// clang-format on
	DECLARE_LOGGER;
	REGISTER_CLASS_INDEX(ShortRangePhys,IPhys);
};
REGISTER_SERIALIZABLE(ShortRangePhys);



/********************** Ip2_PdMat_PdMat_ShortRangePhys ****************************/
class Ip2_PdMat_PdMat_ShortRangePhys : public IPhysFunctor{
	public:
	virtual void go(const shared_ptr<Material>& m1, const shared_ptr<Material>& m2, const shared_ptr<Interaction>& in);
	FUNCTOR2D(PdMat,PdMat);
	// clang-format off
	YADE_CLASS_BASE_DOC_ATTRS(Ip2_PdMat_PdMat_ShortRangePhys,IPhysFunctor,"Short range interactions. Used in the contact law Law2_ScGeom_ShortRangePhys_ShortRangeForce.",
	);
    DECLARE_LOGGER;
	// clang-format on
};
REGISTER_SERIALIZABLE(Ip2_PdMat_PdMat_ShortRangePhys);


/********************** Law2_ScGeom_ShortRangePhys_ShortRangeForce ****************************/
class Law2_ScGeom_ShortRangePhys_ShortRangeForce : public LawFunctor{
	public:
	virtual bool go(shared_ptr<IGeom>& geom, shared_ptr<IPhys>& phys, Interaction* interaction);
	FUNCTOR2D(ScGeom,ShortRangePhys);
	// clang-format off
	YADE_CLASS_BASE_DOC_ATTRS(Law2_ScGeom_ShortRangePhys_ShortRangeForce,LawFunctor,"Constitutive law for short range model.",
	);
	// clang-format on
	DECLARE_LOGGER;
};
REGISTER_SERIALIZABLE(Law2_ScGeom_ShortRangePhys_ShortRangeForce);

} // namespace yade

