
/*************************************************************************
*  Copyright (C) 2020 Philipp Hartmann                                   *
*  philipp.hartmann@newcastle.edu.au                                     *
*                                                                        *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/

#include<pkg/dem/PeriMat.hpp>
#include<pkg/dem/ScGeom.hpp>
#include<core/Omega.hpp>
#include<core/Scene.hpp>
#include<pkg/dem/FrictPhys.hpp>
#include <thread>
#include <chrono>

namespace yade { // Cannot have #include directive inside.

YADE_PLUGIN((PDState)(PdMat)(PdPhys)(Ip2_PdMat_PdMat_PdPhys)(Law2_ScGeom_PdPhys_PdDummy));

//PdMat::~PdMat(){}

/********************** Ip2_PdMat_PdMat_PdPhys ****************************/
CREATE_LOGGER(PdPhys);

//PdPhys::~PdPhys(){};

/********************** Ip2_PdMat_PdMat_PdPhys ****************************/

void Ip2_PdMat_PdMat_PdPhys::go(const shared_ptr<Material>& m1, const shared_ptr<Material>& m2, const shared_ptr<Interaction>& interaction){
	LOG_TRACE( "Ip2_PdMat_PdMat_PdPhys::go - create interaction physics" );
    if( interaction->phys ) return;

/*    // In case of frictional contact it would work !
    Real numIteration = scene->iter;
    if(numIteration >= iniSteps)
    {
        Ip2_FrictMat_FrictMat_FrictPhys iPhysFunctor = Ip2_FrictMat_FrictMat_FrictPhys();
        iPhysFunctor.go(m1,m2,interaction);
        std::cout<< "Jop "<< endl;
        PdPhys* phys = dynamic_cast<PdPhys*>(interaction->phys.get());
        if(phys)
        {
            std::cout<< "Got pd physics "<< endl;
        }
        else
        {
            std::cout<< "No Pd physics"<< endl;
        }
        return;
    }  */

    shared_ptr<PdPhys> physics(new PdPhys());
    // Add body IDS in states
    const int id1 = interaction->id1;
    const int id2 = interaction->id2;

    PDState* st1=dynamic_cast<PDState*>(Body::byId(id1,scene).get()->state.get());
    PDState* st2=dynamic_cast<PDState*>(Body::byId(id2,scene).get()->state.get());
    // (void) st1, (void) st2;
      st1->nbInitBonds++;
      st2->nbInitBonds++;

			//std::cout << "Wait" << '\n';
      st1->neighbours.push_back(id2);
      st2->neighbours.push_back(id1);
			//FIXME Not thread safe. ompThreads=1  need to be set in the interaction loop for the first step. Afterwards ompThreads=O.numThreads
    interaction->phys = physics;
		//std::cout << "Generated PD physics" << '\n';
    (void) m1, (void) m2;

}

/********************** Ip2_PdMat_PdMat_PdPhys ****************************/
CREATE_LOGGER(Ip2_PdMat_PdMat_PdPhys);

Real Law2_ScGeom_PdPhys_PdDummy::getCriticalStretch(const shared_ptr<PdMat>& mat)
{
        const Real& K = mat->young/(3.0*(1.0-2.0*mat->poisson));
        const Real& mu = mat->young/(2.0*(1.0+mat->poisson));
        const Real& gc = mat->gc;
        if(gc == 0.0)
        {
            std::cout<< "Critical energy release rate gc is zero for the computation of critical stretch. Please adjust the input"<< endl;
            exit(0);
        }
        const Real& horizon = mat->horizon;
        // critical stretch  for a linear elastic brittle material with a known critical energy release rate cf. PD Theory and its Application p. 120
        const Real& s = std::sqrt(gc/(3*mu+std::pow(3.0/4.0,4)*(K-(5*mu/3.0)))*horizon);
        // second possibility s = std::sqrt((5*gc)/(9.0*K*horizon));
        return s;
}



/********************** Law2_ScGeom_PdPhys_PdDummy ****************************/

CREATE_LOGGER(Law2_ScGeom_PdPhys_PdDummy);

bool Law2_ScGeom_PdPhys_PdDummy::go(shared_ptr<IGeom>& ig, shared_ptr<IPhys>& ip, Interaction* in)
{
	LOG_TRACE( "Law2_ScGeom_PdPhys_PdDummy::go - contact law" );

//     PdPhys* phys = static_cast<PdPhys*>(ip.get());
        //check if fracture is considered
        if(fracture)
        {
//             // check if bond is broken, when this is the case apply frictional contact
//             if(phys->isBroken)
//             {
//                 // apply frictional contact here !
//                  if(friction)
//                  {
//                     const int &id1 = in->getId1();
//                     const int &id2 = in->getId2();
//                     ScGeom* geom = static_cast<ScGeom*>(ig.get());
//                     Body* b1 = Body::byId(id1,scene).get();
//                     Body* b2 = Body::byId(id2,scene).get();
//
//                     /* NormalForce */
//                     phys->normalForce = Vector3r::Zero();
//                     Real Fn = 0;
//                     // penetration depth
//                     const Vector3r xi = b2->state->refPos-b1->state->refPos;
//                     const Vector3r zeta = b2->state->pos-b1->state->pos;
//                     const Real D = zeta.norm()-xi.norm();
//                     if(D<0) // check for penetration, i.e. current distance of particles is smaller than initial one
//                     {
//                         Fn = phys->kn*D;
//                     }
//                     else // no contact means no shear or tangential force
//                     {
//                        phys->shearForce = Vector3r::Zero();
//                       // phys->normalForce = Vector3r::Zero();   automatically done by next line
//                     }
//                     // compute vectorial normal force
//                     phys->normalForce = -Fn*zeta.normalized();
//
//                     /* ShearForce */
//                     Vector3r& shearForce = phys->shearForce;
//                     shearForce = geom->rotate(phys->shearForce);
//                     const Vector3r& incrementalShear = geom->shearIncrement();
//                     shearForce -= phys->ks*incrementalShear;
//                     	/* Mohr-Coulomb criterion */
//                     Real maxFs = phys->FsMax + phys->normalForce.norm()*phys->tangensOfFrictionAngle;
//                     Real scalarShearForce = shearForce.norm();
//
//                     if (scalarShearForce > maxFs)
//                     {
//                         if (scalarShearForce != 0)
//                             shearForce*=maxFs/scalarShearForce;
//                         else
//                             shearForce=Vector3r::Zero();
//                     }
//
//                     Vector3r f = phys->normalForce + shearForce;
//
//                     scene->forces.addForce (id1,-f);
//                     scene->forces.addForce (id2,f);
//                  }
//             }


                // check critical bond stretch and determine if bond breaks
//                 else
//             {
                const shared_ptr<Body> &b1 = Body::byId(in->id1,scene);
                const shared_ptr<Body> &b2 = Body::byId(in->id2,scene);
                const Vector3r &xi = b2->state->refPos-b1->state->refPos;
                const Vector3r &zeta = b2->state->pos-b1->state->pos;
                const Real &stretch = (zeta.norm()-xi.norm())/xi.norm();
                const shared_ptr<PdMat>& mat = YADE_PTR_CAST<PdMat>(b1->material);
                if(stretch>getCriticalStretch(mat))
                {
                    std::cout<< "Bond betwenn partciles "<< in->id1 << " and "<< in->id2 << " is broken"<< endl;
//                     // critical stretch exceeded
//                     phys->isBroken = true;
                    // Change number of broken bonds
                    PDState* st1=dynamic_cast<PDState*>(Body::byId(in->id1,scene).get()->state.get());
                    PDState* st2=dynamic_cast<PDState*>(Body::byId(in->id2,scene).get()->state.get());
                    st1->nbBrokenBonds++;
                    st2->nbBrokenBonds++;

                    // delete neighbours from state
                    auto i = std::find(begin(st1->neighbours), end(st1->neighbours), b2->id);
                    auto j = std::find(begin(st2->neighbours), end(st2->neighbours), b1->id);

                     st1->neighbours.erase(i);
                     st2->neighbours.erase(j);


                    // Delete interaction
                    scene->interactions->erase(in->id1,in->id2);

//                     // Initialize frictional data if friction between broken bonds is considered
//                     if(friction)
//                     {
//                         Real Ra,Rb;//Vector3r normal;
//                         assert(dynamic_cast<GenericSpheresContact*>(in->geom.get()));//only in debug mode
//                         GenericSpheresContact* sphCont=YADE_CAST<GenericSpheresContact*>(in->geom.get());
//                         Ra=sphCont->refR1>0?sphCont->refR1:sphCont->refR2;
//                         Rb=sphCont->refR2>0?sphCont->refR2:sphCont->refR1;
//
//                         const shared_ptr<PdMat>& mat2 = YADE_PTR_CAST<PdMat>(b1->material);
//                         Real Ea 	= mat->young;
//                         Real Eb 	= mat2->young;
//                         Real Va 	= mat->poisson;
//                         Real Vb 	= mat2->poisson;
//                         Real kna = Ea * Ra;
//                         Real knb = Eb * Rb;
//                         Real ksa = kna * Va;
//                         Real ksb = knb * Vb;
//
//                         // half the harmonic average of the two stiffnesses, when (2*Ri*Ei=2*kni) is the stiffness of a contact point on sphere "i"
//                         Real Kn = 2*kna*knb/(kna+knb);
//                         //same for shear stiffness
//                         Real Ks = 2*ksa*ksb/(ksa+ksb);
//                         Real frictionAngle =  math::min(mat->frictionAngle,mat2->frictionAngle);
//                         phys->tangensOfFrictionAngle = math::tan(frictionAngle);
//                         phys->kn = Kn;
//                         phys->ks = Ks;
//                     }

                }
//             }

        }
        else
        {
            // no fracture. Thus, do nothing

        }


 (void) ig, (void) ip, (void) in;
    // Do nothing since it is a dummy law
	return true;
}


} // namespace yade
