//#ifdef YADE_PD
#pragma once

#include<core/PartialEngine.hpp>
#include<pkg/dem/NumericalMethodEngine.hpp>
#include<pkg/dem/NewtonIntegrator.hpp>
#include<map>

namespace yade { // Cannot have #include directive inside.


class CouplingEngine: public PartialEngine{
  public:
    virtual void action();
    void action_conformingStaggered();
    void action_nonConformingStaggered();

    void initialise();
    void resetGlobalForces(); // Same as force resetter
    //void adjustTimeSteps();
    //bool globalTimeStepIsSmallest();

    // update hybrid particles (new interactions might lead to new hybrid particles. Deleted interaction lead to removal of hybrid particles)
    // function is working but not required atm
    void updateHybridParticles();

    // Functions required for multi-scale time integration
    // set velocities of particles to zero and store them in maps

    // store and set DEM velocities to zero
    void deactivateDEMparticles();

    // set NME velocities to v n-1/2
    void activateNMEparticles();

    // store and set NME (PD) velocities to 0
    void deactivateNMEparticles();
    // restore DEM velocities from last DEM integration
    void activateDEMparticles();

    // // Wrong ! hybrid particle velocities do not have to be reset !
    // // set hybrid velocities to last update since they are overwritten by activateNMEparticles
    void restoreHybridVelNME();
    // hybrid particle velocities (DEM) need to be set to values of last update
    void restoreHybridVelDEM();

    std::vector<shared_ptr<NumericalMethodEngine>> engines; // list of engines with possible different time steps
    shared_ptr<NewtonIntegrator> integrator;

    std::map<int,Vector3r> storedVelDEM; // BodyId, associated velocity
    std::map<int,std::vector<int>> hybridLists; // NME_ID, list of hybrid particles (BodyIDs)

    //MatrixXr forcesFixed;
    Real globalTimeStep = 0.0; // DEM time step
	// clang-format off
  YADE_CLASS_BASE_DOC_ATTRS(CouplingEngine,PartialEngine,"Apply given torque (momentum) value at every subscribed particle, at every step. ",)
	// clang-format on
};
REGISTER_SERIALIZABLE(CouplingEngine);


} // namespace yade

//#endif
