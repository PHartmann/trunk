#include<pkg/dem/ShortRange.hpp>
#include<pkg/dem/PeriMat.hpp>

namespace yade { // Cannot have #include directive inside.


YADE_PLUGIN((ShortRangePhys)(Ip2_PdMat_PdMat_ShortRangePhys)(Law2_ScGeom_ShortRangePhys_ShortRangeForce));

void Ip2_PdMat_PdMat_ShortRangePhys::go(const shared_ptr<Material>& /*m1*/, const shared_ptr<Material>& /*m2*/, const shared_ptr<Interaction>& interaction){
	// phys already exists
	if (interaction->phys) return;
    std::cout<< "New contact"<< endl;
	shared_ptr<ShortRangePhys> phys(new ShortRangePhys()); 
	interaction->phys = phys;
}
CREATE_LOGGER(Ip2_PdMat_PdMat_ShortRangePhys);

/********************** ShortRangePhys ****************************/
CREATE_LOGGER(ShortRangePhys);


/********************** Law2_ScGeom_ShortRangePhys_ShortRangeForce ****************************/
bool Law2_ScGeom_ShortRangePhys_ShortRangeForce::go(shared_ptr<IGeom>& geom, shared_ptr<IPhys>& phys, Interaction* in){
    
    const int &id1 = in->getId1();
    const int &id2 = in->getId2();
    const shared_ptr<Body> &b1 = Body::byId(in->id1,scene);
    const shared_ptr<Body> &b2 = Body::byId(in->id2,scene);
    const Vector3r &zeta = b2->state->pos-b1->state->pos;
    
    const shared_ptr<PdMat>& mat = YADE_PTR_DYN_CAST<PdMat>(b1->material); 
    const Real& K = mat->young/(3.0*(1.0-2.0*mat->poisson));     // compression modulus
    const Real& c = (18.0*K)/(M_PI*std::pow(mat->horizon,4.0));  //micro-modulus
    const Real csh = 5.0*c;
    
    ScGeom* geomSc=static_cast<ScGeom*>(geom.get());
    const Real twoR = geomSc->refR1+geomSc->refR2;
    const Real pen = (zeta.norm()/twoR)-1.0;
    if(pen>=0)
        return false;
//     const Vector3r f = (zeta/zeta.norm())*math::min(0,csh*((zeta.norm()/twoR)-1.0));
    const Vector3r f = (zeta/zeta.norm())*csh*((zeta.norm()/twoR)-1.0);
    
    scene->forces.addForce (id1,f);
    scene->forces.addForce (id2,-f);              
	return true;
    (void) phys;
}
CREATE_LOGGER(Law2_ScGeom_ShortRangePhys_ShortRangeForce);

} // namespace yade

