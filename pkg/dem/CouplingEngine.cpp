//#ifdef YADE_PD
#include<pkg/dem/CouplingEngine.hpp>
#include<pkg/dem/NumericalMethodEngine.hpp>
#include<pkg/dem/NewtonIntegrator.hpp>
#include<pkg/common/InteractionLoop.hpp>
#include<core/Scene.hpp>
#include <iostream>
#include<map>
#include<core/Scene.hpp>
#include<core/State.hpp>

namespace yade { // Cannot have #include directive inside.
    YADE_PLUGIN((CouplingEngine));

void CouplingEngine::action()
{
  //std::cout << "start coupling" << '\n';
  // check if initialisation is necessary and add engines if required
  initialise();


  // check what kind of coupling will be performed + catch possible errors
  int coupling;
  if((int) engines.size()==0)
  {
      throw runtime_error("No NumericalMethodEngine used. Please remove coupling");
  }
  else
  {
    // make sure same coupling is performed for all methods
    coupling = engines[0]->coupling;
    for(int i=1;i< (int) engines.size();i++)
    {
      if(coupling != engines[i]->coupling)
      throw runtime_error("Different coupling strategies are choosen for used NumericalMethodEngine. Not implemented yet");
    }
  }
  // call associated method
  switch(coupling)
  {
    case 1:
      action_conformingStaggered();
      break;
    case 2:
      action_nonConformingStaggered();
      break;
    default:
      COUPLEMODELFUNCDESCR
  }
}

void CouplingEngine::action_nonConformingStaggered()
{
  throw runtime_error("Non-conforming staggered coupling scheme has not been implemented yet");
}



void CouplingEngine::action_conformingStaggered()
{
  // Perform DEM time integration
  integrator->dead = false;

  // set NME velocities to zero

  integrator->action();

  // store DEM forces
  const int nB = (int)scene->bodies->size();
  MatrixXr forcesDEM=MatrixXr::Zero(nB,3);
  for(int i=0;i< nB;i++)
  {
    forcesDEM.row(i)= scene->forces.getForce(i);
  }

  //int const m1 = storedVelDEM.size();
  //int const m2 = engines[0]->storedVel.size();
  //std::cout << "Number DEM"<< m1<< "Number PD"<< m2  << '\n';

  // // update hybrid particles (due to DEM time step new interactions might have been generated)
  // // not needed for integration
  updateHybridParticles();
  // store and set DEM velocities to zero
  deactivateDEMparticles();
  // set NME velocities to v n-1/2
  activateNMEparticles();
  // wrong, not neccessary! set hybrid velocities to last update since they are overwritten by activateNMEparticles
  restoreHybridVelDEM();

  // delete all existing forces within the scene (= DEM forces + hybrid forces based on DEM computations)
  resetGlobalForces();
  // Loop over all engines (= coupled methods)
  for(int i=0;i< (int) engines.size();i++)
  {
    if(engines[i]->timeStepFactorSmaller==1) // smaller is unchanged, i.e. either higher or same as global
    {
        // check if forces and positions have to be updated
        if(engines[i]->stepsUntilForceUpdate == 0)
        {
          //std::cout << "Update: same or higher time step" << '\n';
          //update forces
          engines[i]->updateForces();
          // update stepsUntilForceUpdate
          engines[i]->stepsUntilForceUpdate = engines[i]->timeStepFactorHigher;
          // Add forces
          MatrixXr currentForce = engines[i]->forces;
          for(int j=0;j<currentForce.rows();j++)
          {
            scene->forces.addForce(j,currentForce.row(j));
          }
          scene->dt = engines[i]->timeStepSize;
          integrator->action();
          // set forces back to zero for next engine
          resetGlobalForces();
          scene->dt = globalTimeStep;
        }
        engines[i]->stepsUntilForceUpdate-=1;
      }
  }

  // Loop over all engines (= coupled methods), now for time steps smaller than global one
  for(int i=0;i< (int) engines.size();i++)
  {
    if(engines[i]->timeStepFactorSmaller>1)
    {
      // get number of iterations
      int num =  engines[i]->timeStepFactorSmaller;
      // manipulate time step
      scene->dt = engines[i]->timeStepSize;
      // perform incremental force and kinematic update num-times
      for(int j=0; j< num;j++)
      {
        //std::cout << "Update: smaller time step" << '\n';
        //update forces
        engines[i]->updateForces();
        // Add forces
        MatrixXr currentForce = engines[i]->forces;
        // Add forces to scene
        for(int k=0;k<currentForce.rows();k++)
        {
          scene->forces.addForce(k,currentForce.row(k));
        }
        // Incremental kinematic update
        integrator->action();
        // set forces back to zero for next engine
        resetGlobalForces();
      }
      // reset global time step
      scene->dt = globalTimeStep;
    }
  }

// store and set NME (PD) velocities to 0
deactivateNMEparticles();
// restore DEM velocities from last DEM integration
activateDEMparticles();
// wrong, not neccessary! hybrid particle velocities (DEM) need to be set to values of last update
restoreHybridVelNME();

//std::cout << forcesDEM << '\n';
// Add accumulated forces to scene for postprocessing

  for(int i=0;i<forcesDEM.rows();i++)
    scene->forces.addForce(i,forcesDEM.row(i));
  for(int i=0;i< (int) engines.size();i++)
    engines[i]->addForcesToScene();

  scene->forces.sync();
  integrator->dead = true;
}

void CouplingEngine::initialise()
{
 // check if global timeStep  has changed/been initialised and adjust
 if(globalTimeStep == 0.0 || abs(scene->dt-globalTimeStep)>std::pow(10,-10))
 {
   globalTimeStep =scene->dt;
 }

 // get number of engines used
 int num = scene->engines.size();
 //Get Newton Integrator if necessary
 if(!integrator)
 {
     for(int i=0;i<num;i++)
     {
       NewtonIntegrator* engine = dynamic_cast<NewtonIntegrator*>(scene->engines[i].get());
       if(engine)
       {
          integrator =  YADE_PTR_CAST<NewtonIntegrator>(scene->engines[i]);
          // std::cout << "Newton Integrator is added" << '\n';
       }
     }
     // Double check to make sure an integrator is used
      if(!integrator)
      {
        std::cout<<"\033[1;31mNewtonIntegrator missing in engines. Add an integrator : Exit code \033[0m\n"<<std::endl;
        exit(0);
      }
 }

//Update engine list when new NumericalMethodEngine was added
  for(int i=0;i<num;i++)
  {
  // check which engines are partial engines (change to special engines)
          NumericalMethodEngine* engine = dynamic_cast<NumericalMethodEngine*>(scene->engines[i].get());
          if(engine)
          {
               // std::cout<< "Numerical Method Engine"<< endl;
               // Add engine
               const shared_ptr<NumericalMethodEngine> p =  YADE_PTR_CAST<NumericalMethodEngine>(scene->engines[i]);
               // Check if pointer has already been added into the vector of numerical method engines
               if(p->initialisedInCouplingEngine)
               {
                 continue;
               }
               else
               {
               // Take care about time step size (set factors or time step size)
               p->setTimeStepInformation();
               p->initialisedInCouplingEngine = true;
               p->initialiseFixedConnections();
               engines.push_back(p);
               }
          }
          else
          {
               // std::cout<< "No  Numerical Method Engine"<< endl;
          }
  }

  // Check if DEM particles have been added
  const int numDEM = storedVelDEM.size(); //number of current DEM particles
  int numNME = 0; // get Number of particles in numerical method engines
  for(int i=0;i< (int) engines.size();i++)
  {
    numNME+=engines[i]->numFixedConnections.size();
  }
  const int nB= (int)scene->bodies->size(); // number of Bodies
  int const rest = nB-numDEM-numNME;
  if(rest!=0)
  {
    // get all DEM particles as differences from all bodies and NME bodies
    std::vector<int> bodiesNME;
    std::map<int,int>::iterator it;
    for(int i=0;i< (int) engines.size();i++)
    {
        for(it=engines[i]->numFixedConnections.begin(); it!=engines[i]->numFixedConnections.end(); it++)
        {
          bodiesNME.push_back(it->first);
        }
    }

    // store DEM particles and their current velocities
    for(int i=0; i<nB; i++)
    {
      // check if body i is in list, i.e. is related to a NME
      if(std::find(bodiesNME.begin(), bodiesNME.end(), i) != bodiesNME.end())
      {
        // Element in vector.-> do nothing
      }
      else
      {
        // Body is DEM particle. Thus, store id with associated velocity
        storedVelDEM[i]=(*scene->bodies)[i]->state->vel;
      }
    }

  }


// adjust time steps if they are not compatible
//adjustTimeSteps();
}

void CouplingEngine::resetGlobalForces() // Same as force resetter
{
  scene->forces.reset(scene->iter);
  if(scene->trackEnergy) scene->energy->resetResettables();
}


void CouplingEngine::updateHybridParticles()
{
  for(int i=0;i< (int) engines.size();i++)
  {
    hybridLists[i] = engines[i]->getHybridParticles();
  }

}

void CouplingEngine::deactivateDEMparticles()
{
  std::map<int,Vector3r>::iterator it;
  for(it=storedVelDEM.begin(); it!=storedVelDEM.end(); it++)
  {
    // store velocity
    it->second = (*scene->bodies)[it->first]->state->vel;
    // set velocity to zero
    (*scene->bodies)[it->first]->state->vel = Vector3r::Zero();
  }
}

void CouplingEngine::activateNMEparticles()
{
  std::map<int,Vector3r>::iterator it;
  for(int i=0;i< (int) engines.size();i++)
  {
    for(it=engines[i]->storedVel.begin(); it!=engines[i]->storedVel.end(); it++)
    {
      // restore velocity
      (*scene->bodies)[it->first]->state->vel = it->second;
    }

  }
}

void CouplingEngine::deactivateNMEparticles()
{
  std::map<int,Vector3r>::iterator it;
  for(int i=0;i< (int) engines.size();i++)
  {
      for(it=engines[i]->storedVel.begin(); it!=engines[i]->storedVel.end(); it++)
      {
        // store velocity
        it->second = (*scene->bodies)[it->first]->state->vel;
        // set velocity to zero
        (*scene->bodies)[it->first]->state->vel = Vector3r::Zero();
      }

  }


}

void CouplingEngine::activateDEMparticles()
{
  std::map<int,Vector3r>::iterator it;
  for(it=storedVelDEM.begin(); it!=storedVelDEM.end(); it++)
  {
      // restore velocity
      (*scene->bodies)[it->first]->state->vel = it->second;
  }


}


void CouplingEngine::restoreHybridVelNME()
{
  std::map<int,std::vector<int>>::iterator it;
  // Loop over all hybridLists
  for(it=hybridLists.begin(); it!=hybridLists.end(); it++)
  {
       std::vector<int> hybrids = it->second;
       const int engineNum = it->first;
       // restore velocities for all hybrid particles of respective engine
       for(int i : hybrids)
       {
         (*scene->bodies)[i]->state->vel = engines[engineNum]->storedVel[i];
       }
  }

}

void CouplingEngine::restoreHybridVelDEM()
{
  std::map<int,std::vector<int>>::iterator it;
  // Loop over all hybridLists
  for(it=hybridLists.begin(); it!=hybridLists.end(); it++)
  {
       std::vector<int> hybrids = it->second;
       // restore DEM velocities for all hybrid particles of respective engine
       for(int i : hybrids)
       {
         (*scene->bodies)[i]->state->vel = storedVelDEM[i];
       }
  }

}









// void CouplingEngine::adjustTimeSteps()
// {
//  // add possibility for variable global time step (adjust bigger ones?)
//  //FIXME
//
//  // check if time steps are compatible with each other (only necessary if at least one NumericalMethodEngine time step is smaller than global one)
//    if(!globalTimeStepIsSmallest())
//    {
//      // get smallest time step
//      Real smallestDt = engines[0]->timeStepSize;
//      int pos = 0;
//      for(int i=1; i< (int) engines.size();i++)
//      {
//        if(engines[i]->timeStepSize<smallestDt)
//        {
//          smallestDt = engines[i]->timeStepSize;
//          pos = i;
//        }
//      }
//     // adjust minimal time step if required
//     Real rest = fmod(engines[pos]->timeStepSize,smallestDt);
//     if(rest>std::pow(10,-15))
//     {
//       engines[pos]->timeStepSize-=rest;
//     }
//     const Real fac = globalTimeStep/engines[pos]->timeStepSize;
//     engines[pos]->timeStepFactorSmaller = (int) fac;
//     engines[pos]->stepsUntilForceUpdate = engines[pos]->timeStepFactorSmaller;
//
//      //Check compatibility of all time steps with respect to global time step
//      for(int i=0; i< (int) engines.size();i++)
//      {
//        // only time steps smaller than global one have to be considered at first !!!
//        if(i != pos && engines[i]->timeStepFactorSmaller>1)
//        {
//          // Adjustment of time step if required
//          const Real fac2 = globalTimeStep/engines[i]->timeStepSize;
//          engines[i]->timeStepFactorSmaller = (int) fac2;
//          // reuse this integer to store how often within the loop the engine has to be updated
//          engines[i]->stepsUntilForceUpdate = (int) engines[pos]->timeStepFactorSmaller/engines[i]->timeStepFactorSmaller;
//        }
//      }
//
//    }
// }

// bool CouplingEngine::globalTimeStepIsSmallest()
// {
//   for(int i=0; i< (int) engines.size();i++)
//   {
//     if(engines[i]->timeStepFactorSmaller!=1)
//     {
//       return false;
//     }
//   }
//   return true;
// }

} // namespace yade

//#endif
