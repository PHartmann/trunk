//#ifdef YADE_PD
#include<pkg/dem/PDEngine.hpp>
#include<pkg/dem/PeriMat.hpp>
#include<core/Scene.hpp>
#include<core/State.hpp>
#include <iostream>
#include <chrono>
#include<map>

namespace yade { // Cannot have #include directive inside.
    YADE_PLUGIN((PDEngine));

void PDEngine::updateForces()
{
  //std::cout << "Update PD forces" << '\n';
  //Resize internal force vector to number of particles
  const int nB = (int)scene->bodies->size();
  forces = MatrixXr::Zero(nB,3);
//  std::cout << "Compute Peridynamic forces" << '\n';
      //FIXME loop over all materials and check for fist PD material
        int const numMaterials = scene->materials.size();
        shared_ptr<PdMat> mat;

        // get material information being required for force computations
        for(int i=0;i<=numMaterials;i++)
        {
         if(dynamic_cast<PdMat*>(scene->materials[i].get()))
         {
          mat = YADE_PTR_CAST<PdMat>(scene->materials[i]);
          break;
         }
        }

        // implement different force computations with respect of the chosen material model
        if(this->appliedPDModel==BondBased) // Bond based linear elasticity model
        {
          computeBondBasedForces(mat);
        }
        else if(this->appliedPDModel==StateBased) // Bond based linear elasticity model
        {
          computeStateBasedForces(mat);
        }
        else if(this->appliedPDModel==StateBasedNL) // Bond based linear elasticity model
        {
           computeStateBasedForcesNL(mat);
           //computeBondBasedForces(mat);
        }
        else
        {
         MODELFUNCDESCR
        }
}

// void PDEngine::action()
// {
//   // check if coupled within CouplingEngine, if not perform normal action as defined in NumericalMethodEngine
//   if(!initialisedInCouplingEngine)
//   {
//     normalAction();
//   }
//   else
//   {
//     // do nothing, everything is handled within CoupledEngine
//   }
//
// }

void PDEngine::computeStateBasedForcesNL(const shared_ptr<PdMat>& mat)
{
        // Use logarithmic model
        const int m2 = 0;
        // Get required material parameter (real parameters)
        const Real& mu = mat->young/(2.0*(1.0+mat->poisson));  // shear modulus
        const Real& lamda = (2.0*mu*mat->poisson)/(1.0-2.0*mat->poisson);
        // compute PD material parameters
        const Real&lamdaBar = lamda-mu;
        const Real&muBar = (5.0/2.0)*mu;

        const int nB=(int)scene->bodies->size(); // hoist container size
        // Initialize container for state-based force density container = initialize Tij_all (size)
        std::vector<MatrixXr> Tij_all; // vector of all force vector states
        const Vector3r ini = Vector3r::Zero();
        for (int i = 0; i < nB; i++)
        {
            Tij_all.push_back(ini);
        }
        // Loop over all bodies/particles/families and add shares of respective force state densities
        #pragma omp parallel for
        for(int i=0; i<nB; i++)
        {
            const shared_ptr<Body>& b1 = (*scene->bodies)[i];
            // check if body is PD body, if not skip
            const PdMat* matCheck =  dynamic_cast<PdMat*>(b1->material.get());
            if(!matCheck)
            {
                continue;
            }

            // get number of bonds
            PDState* st1 = dynamic_cast<PDState*>(Body::byId(i,scene).get()->state.get());
            std::vector<int> neighbours = st1->neighbours;
            int const nn = neighbours.size();

           // initialize required variables, vectors and matrices
            VectorXr x_2; // scalar reference state
            VectorXr eps; // scalar deformation state
            VectorXr c; // scalar extension state
            VectorXr wn; // normalised influence function
            MatrixXr y; // deformation state
            MatrixXr Tij; // force state of particle j(in loop over j)

            Real dilatation=0; // dilatation
            //Real m=0; // weighted volume
            x_2.resize(nn,1);
            c.resize(nn,1);
            eps.resize(nn,1);
            wn.resize(nn,1);
            y.resize(nn,3);
            Tij.resize(nn,3);
            // loop over neighbouring particles
            for(int count=0; count< nn;count++)
            {
                const int nJ = neighbours[count];
                const shared_ptr<Body> b2 = Body::byId(nJ,scene);
                const Real vol = b2->state->mass/mat->density;
                const Vector3r xi = b2->state->refPos-b1->state->refPos;
                const Vector3r zeta = b2->state->pos-b1->state->pos;
                // compute states
                x_2(count) = xi.norm()*xi.norm();
                y.row(count) = zeta;
                wn(count) = 3.0/(vol*nn);
                c(count) = zeta.dot(zeta)/xi.dot(xi);
                // compute strain measure required for material model
                if(m2==0)
                {
                    eps(count)= 0.5*std::log(c(count));
                }
                else
                {
                    eps(count) = (1.0/(2.0*m2))*(std::pow(c(count),m2-1));
                }
                dilatation += eps(count)*wn(count)*vol;
            }

            // loop over all neighbouring particles to compute force states Tij
            for(int count=0;count<nn; ++count)
            {
                //compute force state
                Tij.row(count)= (lamdaBar*dilatation+2.0*muBar*eps(count))*wn(count)*(std::pow(c(count),m2-1)/x_2(count))*y.row(count);
            }
            // Save all force states of particle I
            Tij_all[b1->id] = Tij;
        }
        assembleForces(Tij_all,mat);
        //applyForceBoundriesAsForces(mat->density);
}


void PDEngine::assembleForces(std::vector<MatrixXr> &Tij_all, const shared_ptr<PdMat>& mat)
{
     // Force assembling
        int nIntr=(int)scene->interactions->size(); // hoist container size
        #pragma omp parallel for
        for(int j=0; j<nIntr; j++)
        {
            const shared_ptr<Interaction>& i=(*scene->interactions)[j];
            const shared_ptr<PdPhys>& phys = YADE_PTR_CAST<PdPhys>(i->phys);
            // check if bond = interaction is broken
           // if( (!i->isReal()) || !(phys) || bondBroken(phys))
            if(!(i->isReal()) || !(dynamic_cast<PdPhys*>(i->phys.get())))
                continue;
            else
            {
             //get interacting particles
             const shared_ptr<Body>& bI = (*scene->bodies)[i->id1];
             const shared_ptr<Body>& bJ = (*scene->bodies)[i->id2];
             const int nI = bI->id;
             const int nJ = bJ->id;
             const Real volI = bI->state->mass/mat->density;
             const Real volJ = bJ->state->mass/mat->density;
             int pos1 = 0;
             int pos2 = 0;

             PDState* st1 = dynamic_cast<PDState*>(Body::byId(nI,scene).get()->state.get());
             std::vector<int> neighbours1 = st1->neighbours;

             PDState* st2 = dynamic_cast<PDState*>(Body::byId(nJ,scene).get()->state.get());
             std::vector<int> neighbours2 = st2->neighbours;

             for(int i=0;i< (int) neighbours1.size();i++)
             {
               if(neighbours1[i]==nJ)
               {
                 break;
               }
               pos1++;
             }

             for(int i=0;i< (int) neighbours2.size();i++)
             {
               if(neighbours2[i]==nI)
               {
                 break;
               }
               pos2++;
             }
            // std::cout << "pos1: "  <<pos1 << "pos2: "  <<pos2<< '\n';

            // Add forces to container
            // scene->forces.addForce(nI,(Tij_all[nI].row(pos1)-Tij_all[nJ].row(pos2))*volJ*volI);
            // scene->forces.addForce(nJ,(Tij_all[nJ].row(pos2)-Tij_all[nI].row(pos1))*volI*volJ);
            forces.row(nI) = forces.row(nI)+ (Tij_all[nI].row(pos1)-Tij_all[nJ].row(pos2))*volJ*volI;
            forces.row(nJ) = forces.row(nJ)+ (Tij_all[nJ].row(pos2)-Tij_all[nI].row(pos1))*volI*volJ;

           }
        }
}

void PDEngine::computeStateBasedForces(const shared_ptr<PdMat>& mat)
{
        // Get required material parameter
        const Real& K = mat->young/(3.0*(1.0-2.0*mat->poisson));     // compression modulus
        const Real& mu = mat->young/(2.0*(1.0+mat->poisson));

        const int nB=(int)scene->bodies->size(); // hoist container size
        // Initialize container for state-based force density container = initialize Tij_all (size)
        std::vector<MatrixXr> Tij_all; // vector of all force vector states
        const Vector3r ini = Vector3r::Zero();
        auto start = std::chrono::high_resolution_clock::now();
        for (int i = 0; i < nB; i++)
        {
            Tij_all.push_back(ini);
        }
        auto stop1 = std::chrono::high_resolution_clock::now();
        // Loop over all bodies/particles/families and add shares of respective force state densities
        #pragma omp parallel for
        for(int i=0; i<nB; i++)
        {
            const shared_ptr<Body>& b1 = (*scene->bodies)[i];
            // check if body is PD body, if not skip
            const PdMat* matCheck =  dynamic_cast<PdMat*>(b1->material.get());
            if(!matCheck)
            {
                continue;
            }
// //            get number of neighbouring particles
//             PDState* state = dynamic_cast<PDState*>(b1->state.get());
//             int nn = state->neighbours.size();
            // get number of active bonds

            PDState* st1 = dynamic_cast<PDState*>(Body::byId(i,scene).get()->state.get());
            std::vector<int> neighbours = st1->neighbours;
            int const nn = neighbours.size();
           // initialize required variables, vectors and matrices
            VectorXr x_; // scalar reference state
            VectorXr y_; // scalar deformation state
            MatrixXr y; // deformation state
            MatrixXr M; // deformed direction state
            MatrixXr Tij; // force state of particle j(in loop over j)
            VectorXr e; // scalar extension state
            VectorXr e_d; // deviatoric extension state
            VectorXr ts; // scalar for ce densities
            Real theta=0; // dilatation
            Real m=0; // weighted volume
            x_.resize(nn,1);
            y_.resize(nn,1);
            y.resize(nn,3);
            e.resize(nn,1);
            e_d.resize(nn,1);
            ts.resize(nn,1);
            M.resize(nn,3);
            Tij.resize(nn,3);

            for(int count=0; count< nn;count++)
            {
                const int nJ = neighbours[count];
                const shared_ptr<Body> b2 = Body::byId(nJ,scene);
                const Real vol = b2->state->mass/mat->density;
                const Vector3r xi = b2->state->refPos-b1->state->refPos;
                const Vector3r zeta = b2->state->pos-b1->state->pos;
                // compute states
                x_(count) = xi.norm();
                y_(count) = zeta.norm();
                M.row(count) = zeta/zeta.norm();
                e(count) = y_(count)-x_(count);
                // Compute and add share of weighted volume and dilation
                m += x_(count)*x_(count)*vol;
                theta += x_(count)*e(count)*vol;
              //  count++;
            }
            // get correct dilatation
            theta *= (3.0/m);
            // micro modulus
            Real alpha = (15.0*mu)/m;
            // compute deviatoric extension state
            e_d = e-(theta*x_/3.0);
            // compute scalar force densities
            ts = ((3.0*K*theta)/m)*x_+alpha*e_d;

            // loop over all neighbouring particles and compute force states Tij
            for(int count=0;count<nn; count++)
            {
                //compute force state
                Tij.row(count)= ts(count)*M.row(count);
            }
            // Save all force states of particle I
            Tij_all[b1->id] = Tij;
        }
        auto stop2 = std::chrono::high_resolution_clock::now();
        assembleForces(Tij_all,mat);
        auto stop3 = std::chrono::high_resolution_clock::now();

        auto duration1 = std::chrono::duration_cast<std::chrono::seconds>(stop1 - start);
        auto duration2 = std::chrono::duration_cast<std::chrono::seconds>(stop2 - stop1);
        auto duration3 = std::chrono::duration_cast<std::chrono::seconds>(stop3 - stop2);
        //cout << "Time taken to first stop: "<< duration1.count() << "seconds" << endl;
        //cout << "Time taken  from first to second stop: "<< duration2.count() << "seconds" << endl;
        //cout << "Time taken from second to third stop: "<< duration3.count() << "seconds" << endl;
        //applyForceBoundriesAsForces(mat->density);
}


void PDEngine::computeBondBasedForces(const shared_ptr<PdMat>& mat)
{
        // Get required material parameter
        const Real& K = mat->young/(3.0*(1.0-2.0*mat->poisson));     // compression modulus
        const Real& c = (18.0*K)/(M_PI*std::pow(mat->horizon,4.0));  //micro-modulus

        int nIntr=(int)scene->interactions->size(); // hoist container size
        #pragma omp parallel for
        for(int j=0; j<nIntr; j++)
        {
            const shared_ptr<Interaction>& i=(*scene->interactions)[j];
            const shared_ptr<PdPhys>& phys = YADE_PTR_CAST<PdPhys>(i->phys);
            // check if bond = interaction is broken
//             if( (!i->isReal()) || !(phys) || bondBroken(phys))
            if((!i->isReal()) || !(dynamic_cast<PdPhys*>(i->phys.get())))
                continue;
            else // computation of bond based forces
            {
             //get interacting particles
             const shared_ptr<Body>& b1 = (*scene->bodies)[i->id1];
             const shared_ptr<Body>& b2 = (*scene->bodies)[i->id2];
             //compute current  and  initial bond vectors
             const Vector3r xi = b2->state->refPos-b1->state->refPos;
             const Vector3r xe = b2->state->pos-b1->state->pos;
             // compute force density nI->nJ
             const Vector3r dir_bond = xe/xe.norm();
             const Real     f= c*(xe.norm()-xi.norm())/xi.norm();
             const Vector3r t = dir_bond*f;
             const Vector3r force = t*(b1->state->mass/mat->density);
             //add force
             // scene->forces.addForce(i->id1,force);
             // scene->forces.addForce(i->id2,-force);
             forces.row(i->id1) = (Vector3r) forces.row(i->id1)+force;
             forces.row(i->id2) = (Vector3r) forces.row(i->id2)-force;// - force;
            }
        }
       // applyForceBoundriesAsForces(mat->density);
}

// bool PDEngine::bondBroken(const shared_ptr<PdPhys>& physics)
// {
//                  return physics->isBroken;
// }

void PDEngine::applyForceBoundriesAsDensities(std::vector<MatrixXr> &Tij_all)
{
    for(int i=0;i< (int) forceDensityNodes.size();i++)
    {
        // loop over all interactions Tij and apply force densities to all interactions
        for(int j=0;j<Tij_all[forceDensityNodes[i]].rows();j++)
        {
           Tij_all[i].row(j) =  forceDensityApplied;
        }
    }
}

void PDEngine::applyForceBoundriesAsForces(const Real density) //FIXME assumption that the prescriped force is the only one
{
    for(int i=0;i< (int) forceDensityNodes.size();i++)
     {
        const Vector3r force = scene->forces.getForce(i);
        scene->forces.addForce(i,-force); // Force is zero now

        const shared_ptr<Body>& bI = (*scene->bodies)[i];
        scene->forces.addForce(i,(bI->state->mass/density)*forceDensityApplied);
     }
}


// Computation of Deformation gradients and related stresses
void PDEngine::computeStresses(const shared_ptr<PdMat>& mat)
{
        // Get required material parameter
        const Real& K = mat->young/(3.0*(1.0-2.0*mat->poisson));     // compression modulus
        const Real& mu = mat->young/(2.0*(1.0+mat->poisson));  // shear modulus

        const int nB=(int)scene->bodies->size(); // hoist container size
        // Loop over all bodies/particles/families and add shares of respective force state densities
        #pragma omp parallel for
        for(int i=0; i<nB; i++)
        {
            const shared_ptr<Body>& b1 = (*scene->bodies)[i];
            // check if body is PD body, if not skip
            const PdMat* matCheck =  dynamic_cast<PdMat*>(b1->material.get());
            if(!matCheck)
            {
                continue;
            }

            // get number of bonds
            PDState* st1 = dynamic_cast<PDState*>(Body::byId(i,scene).get()->state.get());
            std::vector<int> neighbours = st1->neighbours;
            int const nn = neighbours.size();

           // initialize required variables, vectors and matrices
            VectorXr x_2; // scalar reference state
            VectorXr eps; // scalar deformation state
            VectorXr c; // scalar extension state
            VectorXr wn; // normalised influence function
            MatrixXr y; // deformation state
            MatrixXr Tij; // force state of particle j(in loop over j)

            //Real m=0; // weighted volume
            x_2.resize(nn,1);
            c.resize(nn,1);
            eps.resize(nn,1);
            wn.resize(nn,1);
            y.resize(nn,3);
            Tij.resize(nn,3);
            // loop over neighbouring particles
            for(int count=0; count< nn;count++)
            {
                const int nJ = neighbours[count];
                const shared_ptr<Body> b2 = Body::byId(nJ,scene);
                const Real vol = b2->state->mass/mat->density;
                const Vector3r xi = b2->state->refPos-b1->state->refPos;
                const Vector3r zeta = b2->state->pos-b1->state->pos;
                const Vector3r notUsed = xi+zeta*K*mu*vol;
                (void)notUsed;
            }
          }

}



std::vector<int> PDEngine::getHybridParticles()
{
    std::vector<int> hybrids;
    // loop over all PD bodies
    std::map<int,int>::iterator it;
    for(it=numFixedConnections.begin(); it!=numFixedConnections.end(); it++)
    {
     int const interactionsRef = it->second;
     int const interactionsNow =  Body::byId(it->first,scene)->coordNumber();
     if(interactionsNow>interactionsRef)
     {
      //  std::cout << interactionsNow-interactionsRef << '\n';
        hybrids.push_back(it->first);
     }
    }
    //  std::cout << "Hybrids:" << '\n';
    //  for(int i : hybrids)
    //  {
    //    std::cout << i << '\n';;
    //  }
    //
    // std::cout << "Num Hybrids:" << hybrids.size()<<'\n';
    return hybrids;
}

void PDEngine::initialiseFixedConnections()
{
  const int nB=(int)scene->bodies->size();
  #pragma omp parallel for
  for(int i=0; i<nB; i++)
  {
      const shared_ptr<Body>& b1 = (*scene->bodies)[i];
      // check if body is PD body, if not skip
      const PdMat* matCheck =  dynamic_cast<PdMat*>(b1->material.get());
      if(!matCheck)
      {
          continue;
      }
    // get number of bonds
    PDState* st1 = dynamic_cast<PDState*>(Body::byId(i,scene).get()->state.get());
    std::vector<int> neighbours = st1->neighbours;
    int const nn = neighbours.size();
    // store number of neighbours
    numFixedConnections[i]=nn;
    storedVel[i]= b1->state->vel;
  }


}



} // namespace yade

//#endif
