//#ifdef YADE_PD
#pragma once

#include<pkg/dem/NumericalMethodEngine.hpp>

namespace yade { // Cannot have #include directive inside.

class TestEngine: public NumericalMethodEngine{
  public:
    //virtual void action();
    void updateForces() override final;
	// clang-format off
  YADE_CLASS_BASE_DOC_ATTRS(TestEngine,NumericalMethodEngine,"Apply given torque (momentum) value at every subscribed particle, at every step. ",
  );
	// clang-format on
};
REGISTER_SERIALIZABLE(TestEngine);


} // namespace yade

//#endif
