# -*- coding: utf-8 -*-

"""
Tension test of a bonded particle packing using peridynamics 

Author: Philipp Hartmann
"""
from yade import plot,export,ymport, pack
import numpy as np

#### create output directories
try:
	os.mkdir('gnuplot')
except:
	pass
try:
	os.mkdir('paraview')
except:
	pass

#### PARAMETERS

## PD material parameters
youngPD = 6e12
nuPD = 0.4
densityPD = 1100

#btype = "bending"
btype = "tension"

#### GEOMETRY
sphereRadius = 2.0 #(=0.5*particle spacing)
## Peridynamic quantaties
#interaction radius for creating the initial bonds
m=2
horizonPD = m*(sphereRadius*2) # Real peridynamic horizon 
#intRadius=horizon*1.01
intRadius =(horizonPD/4)*1.01 # divided by 4 since horizon is sphere radius times 2 and for PD the distance of centres of of interest and not contact of spheres 

### MATERIAL
# ADD ELASTIC PD MATERIAL 
O.materials.append(
  PdMat(
  young = youngPD,
  poisson = nuPD,
  density = densityPD,
  horizon = horizonPD,
  label='MatPD'))

sp = pack.regularOrtho(pack.inAlignedBox((0,0,0),(100,50,20)),radius=sphereRadius,gap=0,color=(0,0,1), material = 'MatPD')
#sp = pack.regularOrtho(pack.inAlignedBox((0,0,0),(25,25,5)),radius=sphereRadius,gap=0,color=(0,0,1), material = 'MatSpheres')
O.bodies.append(sp)



#### Boundaries (directions related to regularOrtho (x,y,z)) get all particle of a regular geometry  
# x-axis min, max particles 
bb=uniaxialTestFeatures(axis=0)
minX,maxX = bb['negIds'],bb['posIds']
# y-axis min, max particles 
bb=uniaxialTestFeatures(axis=1)
minY,maxY  = bb['negIds'],bb['posIds']
# z-axis min, max particles 
bb=uniaxialTestFeatures(axis=2)
minZ, maxZ  = bb['negIds'],bb['posIds']

### Extend boundary layers () for PD computations 
# minX 
for i in range (len(minX)*(m-1)):
    minX.append(minX[-1]+1)
# maxX 
maxX.reverse()
for i in range (len(maxX)*(m-1)):
    maxX.append(maxX[-1]-1)    
    

#### UNIAXIAL TEST FEATURE
bb=uniaxialTestFeatures(axis=0)
negIds,posIds,axis,crossSectionArea=bb['negIds'],bb['posIds'],bb['axis'],bb['area']

## change colour of moving particles negIds and posIds
for i in minX:
	O.bodies[i].shape.color=[1,0,0]
for i in maxX:
	O.bodies[i].shape.color=[1,0,0]
	
#print "Number of negative Ids:", len(negIds)reew
#print "Number of positive Ids:", len(posIds)



#### DEFINE SOME FUNCTIONS

# Function replacing the sort collinder since bonds have to be defined only once        
def initializeBonds(intRadius):
    #print "initializeBonds"
    O.step() 
    #numP = 0
    ##loop over all particles 
    #for i in range(len(O.bodies)):
        ## loop over all possible neighbour particles
        #for j in range(len(O.bodies)):
            ## Check if particle distance is within the horizon 
            #if (O.bodies[i].state.pos-O.bodies[j].state.pos).norm() < intRadius and i != j: # 
               ## Add interaction 
               #numP= numP+1

             ##print "Particle:", O.bodies[i].state.pos#i,j
    #print "Number of interactions:", numP
    
    #wrongDetect = 0
    #rightDetect = 0
    
    ### loop over all interactions     
    #for i in O.interactions:
        ##check distance of particles 
        #if (O.bodies[i.id1].state.pos-O.bodies[i.id2].state.pos).norm() <= intRadius:
            #wrongDetect = wrongDetect+12
        #else :
            #rightDetect = rightDetect+1
            
    #print "Greater then:", wrongDetect 
    #print "Smaller then:", rightDetect 
    
    # adjust volumes #FIXME
    
    ## check number of interactions
    num = 0
    for i in O.interactions:
        if i.isReal :
            num = num+1
            #if i.id1==20 or i.id2==20:
               #print i.id1, " ", i.id2
               
    print ("Number of interactions:", num)
    print ("Number of possible interactions:", len(O.interactions))
    print ("Number of particles:", len(O.bodies))

def computeBondBasedForce(horizon): ## only bond based models ! #FIXME 
   # print "Ply runner working"
   # print "Horizon: ", O.materials[0].density
    MatPD = O.materials["MatPD"]
    # Compression modulus 
    K = MatPD.young/(3*(1-2*MatPD.poisson))
    #K= 10000
    c = (18*K)/(pi*pow(horizon,4)) # 18k/pi delta^4
    ## loop over all interactions     ## for state based loop over all bodies and get interactions 
    for i in O.interactions:
        if i.isReal :
         # get interacting particles 
         # compute current  and  initial bond vectors 
          xi = O.bodies[i.id2].state.refPos-O.bodies[i.id1].state.refPos
          xe = O.bodies[i.id2].state.pos-O.bodies[i.id1].state.pos
          # compute force density nI->nJ 
          eta = xi-xe
          dir_bond = xe/xe.norm()
          f = c*(xe.norm()-xi.norm())/xi.norm()
          t = dir_bond*f   
          force = t*(O.bodies[i.id1].state.mass/MatPD.density)
         # add force 
          #print i.id1," " ,i.id2," ",force
          O.forces.addF(i.id1,force)
          O.forces.addF(i.id2,-force)
          
def computeLinearElasticityForce(horizon): ## state based model

    MatPD = O.materials["MatPD"]
    # define peridynamic material parameters   
    K = MatPD.young/(3*(1-2*MatPD.poisson))
    # shear modulus 
    mu = MatPD.young/(2.0*(1+MatPD.poisson))
    
    # List of matrices containing all force states
    T_all = []
    F_u = MatrixX.Zero(len(O.bodies),3)   # matrix of resulting forces per unit volume 
    ## for state based loop over all bodies and get interactions 
    for i in O.bodies:
        # get list of interactions (=contains neighbours) 
        neighbours = i.intrs()
        nn = len(neighbours)
        # initialize neighbouring 
        x_ = VectorX.Zero(nn)    # scalar reference state
        y_ = VectorX.Zero(nn)    # scalar deformation state
        y = MatrixX.Zero(nn,3)   # deformation state
        M = MatrixX.Zero(nn,3)   # deformed direction state
        Tij = MatrixX.Zero(nn,3) # force state of particle i(in loop over j)
        e = VectorX.Zero(nn)     # scalar extension state
        e_d = VectorX.Zero(nn)   # scalar deviatoric extension state
        ts = VectorX.Zero(nn)    # scalar force state
        theta = 0; # dilatation
        m = 0; # weighted volume
    
        count = 0
        # loop over all interactions to compute states 
        for j in neighbours:
            # check for neighbouring body id for computation 
            if j.id1 == i.id:
                # neighbour particle has id2 in interaction
                nJ = O.bodies[j.id2]
            else: 
                # neighbour particle has id1 in interaction 
                nJ = O.bodies[j.id1]
            # Get required information of neighbour particle    
            xi   = nJ.state.refPos-i.state.refPos # initial bond 
            zeta = nJ.state.pos-i.state.pos # current bond 
            # compute states 
            x_[count] = xi.norm()
            y_[count] = zeta.norm()
            M[count] = zeta/zeta.norm() # M[count] is the equivalent of M.row(count) which is not available
            count = count+1 # update counter
        # Compute extension state 
        e = y_-x_  
        # additional loop over all interactions to compute weighted volume 
        count = 0
        # loop over all interactions to compute states 
        for j in neighbours:
            # check for neighbouring body id for computation 
            if j.id1 == i.id:
                # neighbour particle has id2 in interaction
                nJ = O.bodies[j.id2]
            else:
                # neighbour particle has id1 in interaction 
                nJ = O.bodies[j.id1]
            # Get required volume of neighbour particle    
            vol  = nJ.state.mass/MatPD.density # volume 
            # Compute and add share of weighted volume and dilation
            m = m + x_[count]*x_[count]*vol
            theta = theta + x_[count]*e[count]*vol
            count = count+1 #update counter
            
        # get correct dilatation 
        theta = theta*(3.0/m)
        # micro modulus 
        alpha = (15.0*mu)/m    
        # compute deviatoric extension state 
        e_d = e-(theta*x_/3.0)
        # compute scalar force densities 
        ts = ((3.0*K*theta)/m)*x_+alpha*e_d
        
        # loop over all neighbouring particles and compute force states Tij   
        count = 0
        for j in neighbours:
            # compute force state 
            Tij[count] = ts[count]*M[count]
            # update counter 
            count = count+1
        # Save force states of particle I    
        T_all.append(Tij)
        
    # loop over all bodies to assemble forces   
    for i in O.bodies:
        count = 0
        neighbours = i.intrs()
        for j in neighbours:
                # check for neighbouring body id for computation 
            if j.id1 == i.id:
                # neighbour particle has id2 in interaction
                nJ = O.bodies[j.id2]
            else: 
                # neighbour particle has id1 in interaction 
                nJ = O.bodies[j.id1]  
            vol  = nJ.state.mass/MatPD.density # volume  
            # Find position of interaction of particle i in  T_all(particle j) 
            interactions = nJ.intrs()
            pos = 0
            for k in interactions :
                if k.id1 == i.id or k.id2 == i.id :
                    break
                else  :
                    pos = pos +1
            # add force density to particle i , i.e (Tij-Tji)*vol_j
            F_u[i.id] = F_u[i.id] +  (T_all[i.id][count] - T_all[nJ.id][pos])*vol # Integrating Tij-Tji 
            count = count + 1
        O.forces.addF(i.id,F_u[i.id]*(i.state.mass/MatPD.density)) # Add accumulated force for particle i 

def computeNonLinearElasticityForce(horizon): ## state based model
    
    m2 = 0 # define level 
    
    MatPD = O.materials["MatPD"]
    # shear modulus 
    mu = MatPD.young/(2.0*(1+MatPD.poisson))
    
    lamda = (2*mu*MatPD.poisson)/(1.0-2.0*MatPD.poisson)
    lamdaBar = lamda-mu
    muBar = (5.0/2.0)*mu
    
    # List of matrices containing all force states
    T_all = []
    F_u = MatrixX.Zero(len(O.bodies),3)   # matrix of resulting forces per unit volume 
    ## for state based loop over all bodies and get interactions 
    for i in O.bodies:
        # get list of interactions (=contains neighbours) 
        neighbours = i.intrs()
        nn = len(neighbours)
        # initialize neighbouring 
        c = VectorX.Zero(nn)     # state strain measure
        eps = VectorX.Zero(nn)   # state strain measure
        x_2 = VectorX.Zero(nn)   # scalar reference state squared 
        wn  = VectorX.Zero(nn)   # normalized influence function 
        y   = MatrixX.Zero(nn,3) # deformation state
        Tij = MatrixX.Zero(nn,3) # force state of particle i(in loop over j)
        m = 0                    # weighted volume 
        dilatation = 0           # non-linear dilatation
        
        count = 0
        # loop over all interactions to compute states 
        for j in neighbours:
            # check for neighbouring body id for computation 
            if j.id1 == i.id:
                # neighbour particle has id2 in interaction
                nJ = O.bodies[j.id2]
            else: 
                # neighbour particle has id1 in interaction 
                nJ = O.bodies[j.id1]
            # Get required information of neighbour particle   
            # Get required volume of neighbour particle    
            vol  = nJ.state.mass/MatPD.density # volume 
            xi       = nJ.state.refPos-i.state.refPos # initial bond 
            zeta = nJ.state.pos-i.state.pos # current bond 
            y[count] = zeta
            # compute PD quantaties
            xixi = xi.dot(xi)
            yy = zeta.dot(zeta)
            x_2[count] = xi.norm()*xi.norm()
            wn[count] = 3.0/(vol*nn)
            m = m + xixi*vol 
            c[count] = yy/xixi
            count = count+1 # update counter
        # additional loop over all interactions to compute strain measure and dilatation 
        count = 0
        for j in neighbours:
            if j.id1 == i.id:
                nJ = O.bodies[j.id2]
            else: 
                nJ = O.bodies[j.id1] 
            vol  = nJ.state.mass/MatPD.density # volume 
            # compute strain 
            if(m2==0): # logarithmic model 
                eps[count] = 0.5*log(c[count]) 
            else:
                eps[count] = (1.0/(2.0*m2))*(c[count].pow(m2)-1)
            # accumulate dilatation     
            dilatation = dilatation + eps[count]*wn[count]*vol 
            count = count+1 #update counter
        # loop over all neighbouring particles and compute force states Tij   
        count = 0
        for j in neighbours:
            # compute force state 
            Tij[count] = (lamdaBar*dilatation+2.0*muBar*eps[count])*wn[count]*(pow(c[count],m-1)/x_2[count])*y[count]
            # update counter 
            count = count+1
        # Save force states of particle I    
        T_all.append(Tij)
    # loop over all bodies to assemble forces   
    for i in O.bodies:
        count = 0
        neighbours = i.intrs()
        for j in neighbours:
                # check for neighbouring body id for computation 
            if j.id1 == i.id:
                # neighbour particle has id2 in interaction
                nJ = O.bodies[j.id2]
            else: 
                # neighbour particle has id1 in interaction 
                nJ = O.bodies[j.id1]  
            vol  = nJ.state.mass/MatPD.density # volume  
            # Find position of interaction of particle i in  T_all(particle j) 
            interactions = nJ.intrs()
            pos = 0
            for k in interactions :
                if k.id1 == i.id or k.id2 == i.id :
                    break
                else  :
                    pos = pos +1
            # add force density to particle i , i.e (Tij-Tji)*vol_j
            F_u[i.id] = F_u[i.id] +  (T_all[i.id][count] - T_all[nJ.id][pos])*vol # Integrating Tij-Tji 
            count = count + 1
        O.forces.addF(i.id,F_u[i.id]*(i.state.mass/MatPD.density)) # Add accumulated force for particle i 

def getBoundaries(btype):
    if(btype=="bending"):
        print("Computation of bending problem")
        # Loop over all boundary particles in x-direction
        for i in minX:
            O.bodies[i].state.blockedDOFs = 'xyz'
        for i in maxX:
            O.bodies[i].state.blockedDOFs = 'xyz'
    elif(btype=="punch"):
        print("Computation of punch problem")
    elif(btype=="tension"):
        print("Computation of tension problem")   
        # Loop over all boundary particles in x-direction
        for i in minX:
            O.bodies[i].state.blockedDOFs = 'xyz'
        for i in maxX:
            O.bodies[i].state.blockedDOFs = 'xyz'
    else:
        print("Boundaries not defined")
    # not implemented yet     

def applyBoundaries(btype):
    if(btype=="bending"):
        vel = 50
        for i in maxX:
            O.bodies[i].state.vel[1] =  -vel
            
    elif(btype=="punch"):
        a=2
    elif(btype=="tension"):
        # Define velocity 
        #disp = strainRate*abs(O.bodies[maxX[0]].state.refPos[0]-O.bodies[minX[0]].state.refPos[0])
        vel = 50
        for i in minX:
            #O.bodies[i].state.pos[0] = O.bodies[i].state.pos[0] - disp
            O.bodies[i].state.vel[0] = -vel
        for i in maxX:
            #O.bodies[i].state.pos[0] = O.bodies[i].state.pos[0] + disp
            O.bodies[i].state.vel[0] =  vel
    else:
        print("btype not found")
        a=2 

#### ENGINES 
O.engines=[
	ForceResetter(),
	InsertionSortCollider([Bo1_Sphere_Aabb(aabbEnlargeFactor=intRadius,label='is2aabb')]),
	InteractionLoop(
		[Ig2_Sphere_Sphere_ScGeom(interactionDetectionFactor=intRadius,label='ss2sc')],
		[Ip2_PdMat_PdMat_PdPhys()],
		[Law2_ScGeom_PdPhys_PdDummy()]),
    PDEngine(appliedPDModel=3),
    #PyRunner(iterPeriod=1,initRun=False,command='computeBondBasedForce(horizon)',label='PD_force_computation'), #FIXME  
    #PyRunner(iterPeriod=1,initRun=False,command='computeLinearElasticityForce(horizon)',label='PD_SB_force_computation'), #FIXME  
    #PyRunner(iterPeriod=1,initRun=False,command='computeNonLinearElasticityForce(horizon)',label='PD_SB_force_computation'), #FIXME 
    #PyRunner(iterPeriod=100,initRun=False,command='computeStresses()',label='stressComputation'),
    #Apply boundary conditions manually 
    PyRunner(iterPeriod=1,initRun=False,command='applyBoundaries(btype)',label='boundayApplication'),
    # Solve dynamics
    NewtonIntegrator(label='newton'), 
]
O.dt=1e-5
       

       
       
##Calculate number of interactions initially
#O.step()
#totBond = init()
##Peridynamic initialisation
initializeBonds(intRadius)
#strainer.dead=False
is2aabb.aabbEnlargeFactor=0
ss2sc.interactionDetectionFactor=0

#Define boundary particles globally  
getBoundaries(btype)

yade.qt.Controller()
yade.qt.View()
renderer=yade.qt.Renderer()
renderer.dispScale=(100,1,0)  # scale displacement in GUI
renderer.bgColor=(1,1,1)


#plot.plots={'eps':('sigma')}
#plot.plot(noShow = False, subPlots=False)

O.saveTmp()
